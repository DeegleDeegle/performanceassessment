package com.example.student.performanceassessment;


import android.net.Uri;

public interface ActivityCallback {
    void onPostSelected(Uri redditPostUri);
}
