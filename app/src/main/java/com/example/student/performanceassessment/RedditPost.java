package com.example.student.performanceassessment;


public class RedditPost {

    public String title;
    public String date;
    public String due;
    public String work;



    public RedditPost(String title, String date, String due, String work) {
        this.title = title;
        this.date = date;
        this.due = due;
        this.work = work;
    }
}
