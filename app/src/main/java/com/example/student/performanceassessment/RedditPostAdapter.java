package com.example.student.performanceassessment;


import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class RedditPostAdapter extends RecyclerView.Adapter<RedditPostHolder> {

    public String[] RedditPosts;

    public RedditPostAdapter(String[] redditPosts){
        this.RedditPosts = redditPosts;
    }

    @Override
    public RedditPostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new RedditPostHolder(view);
    }


    @Override
    public void onBindViewHolder(RedditPostHolder holder, final int i) {
        holder.titleText.setText(RedditPosts[i]);

    }
    @Override
    public int getItemCount() {

        return RedditPosts.length;
    }
}
